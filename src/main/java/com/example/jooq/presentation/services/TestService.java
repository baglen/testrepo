package com.example.jooq.presentation.services;

import com.example.jooq.domain.usecase.CreateTestUseCaseImpl;
import com.example.jooq.domain.usecase.CreateUseCaseImpl;
import com.example.jooq.models.tables.pojos.Test;
import com.example.jooq.models.tables.pojos.TestTestfield;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;


@Service
public class TestService {

    private final DSLContext context;

    public TestService(DSLContext context) {
        this.context = context;
    }

    public Test create(String text){
        return new CreateUseCaseImpl().execute(context, text);
    }

    public TestTestfield createTest(Long testId){
        return new CreateTestUseCaseImpl().execute(context, testId);
    }


}
package com.example.jooq.presentation.controllers;

import com.example.jooq.models.tables.pojos.Test;
import com.example.jooq.models.tables.pojos.TestTestfield;
import com.example.jooq.presentation.services.TestService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @PostMapping(value = "/create")
    public Test create(@RequestParam("text") String text){
        return testService.create(text);
    }

    @PostMapping(value = "/createTest")
    public TestTestfield createTest(@RequestParam("id") Long id){
        return testService.createTest(id);
    }
}

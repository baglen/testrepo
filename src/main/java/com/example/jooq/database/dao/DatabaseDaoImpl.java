package com.example.jooq.database.dao;

import com.example.jooq.models.Tables;
import com.example.jooq.models.tables.pojos.Test;
import com.example.jooq.models.tables.pojos.TestTestfield;
import org.jooq.DSLContext;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public class DatabaseDaoImpl implements DatabaseDao{

    @Override
    public Test create(DSLContext context, String text) {
        return context
                .insertInto(Tables.TEST)
                .set(Tables.TEST.TEXT, text)
                .set(Tables.TEST.CREATED, OffsetDateTime.now())
                .returning()
                .fetchOne()
                .map(r -> {
                    Test test = new ModelMapper().map(r, Test.class);
                    return test;
                });
    }

    @Override
    public TestTestfield createTest(DSLContext context, Long testId) {
        return context
                .insertInto(Tables.TEST_TESTFIELD)
                .set(Tables.TEST_TESTFIELD.TEST_ID, testId)
                .set(Tables.TEST_TESTFIELD.CREATED, OffsetDateTime.now())
                .returning()
                .fetchOne()
                .map(r -> {
                    TestTestfield testTestfield = new ModelMapper().map(r, TestTestfield.class);
                    return testTestfield;
                });
    }
}

package com.example.jooq.database.dao;

import com.example.jooq.models.tables.pojos.Test;
import com.example.jooq.models.tables.pojos.TestTestfield;
import org.jooq.DSLContext;

public interface DatabaseDao {
    Test create(DSLContext context, String text);
    TestTestfield createTest(DSLContext context,Long testId);
}

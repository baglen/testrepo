package com.example.jooq.database.datasource;

import com.example.jooq.data.datasource.DatabaseDataSource;
import com.example.jooq.database.dao.DatabaseDao;
import com.example.jooq.database.dao.DatabaseDaoImpl;
import com.example.jooq.models.tables.pojos.Test;
import com.example.jooq.models.tables.pojos.TestTestfield;
import org.jooq.DSLContext;

public class DatabaseDataSourceImpl implements DatabaseDataSource {

    private DatabaseDao databaseDao = new DatabaseDaoImpl();

    @Override
    public Test create(DSLContext context, String text) {
        return databaseDao.create(context, text);
    }

    @Override
    public TestTestfield createTest(DSLContext context, Long testId) {
        return databaseDao.createTest(context, testId);
    }
}

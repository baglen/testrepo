package com.example.jooq.domain.usecase;

import com.example.jooq.models.tables.pojos.TestTestfield;
import org.jooq.DSLContext;

public interface CreateTestUseCase {
    TestTestfield execute(DSLContext context, Long testId);
}

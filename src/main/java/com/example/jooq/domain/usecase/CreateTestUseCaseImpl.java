package com.example.jooq.domain.usecase;

import com.example.jooq.data.repository.DatabaseRepositoryImpl;
import com.example.jooq.domain.repository.DatabaseRepository;
import com.example.jooq.models.tables.pojos.TestTestfield;
import org.jooq.DSLContext;

public class CreateTestUseCaseImpl implements CreateTestUseCase{

    private DatabaseRepository databaseRepository = new DatabaseRepositoryImpl();

    @Override
    public TestTestfield execute(DSLContext context, Long testId) {
        return databaseRepository.createTest(context, testId);
    }
}

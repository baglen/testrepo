package com.example.jooq.domain.usecase;

import com.example.jooq.data.repository.DatabaseRepositoryImpl;
import com.example.jooq.domain.repository.DatabaseRepository;
import com.example.jooq.models.tables.pojos.Test;
import org.jooq.DSLContext;

public class CreateUseCaseImpl implements CreateUseCase{

    private DatabaseRepository databaseRepository = new DatabaseRepositoryImpl();

    @Override
    public Test execute(DSLContext context, String text) {
        return databaseRepository.create(context, text);
    }
}

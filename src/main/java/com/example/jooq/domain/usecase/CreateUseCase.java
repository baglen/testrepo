package com.example.jooq.domain.usecase;

import com.example.jooq.models.tables.pojos.Test;
import org.jooq.DSLContext;

public interface CreateUseCase {
    Test execute(DSLContext context, String text);
}

package com.example.jooq.data.repository;

import com.example.jooq.data.datasource.DatabaseDataSource;
import com.example.jooq.database.datasource.DatabaseDataSourceImpl;
import com.example.jooq.domain.repository.DatabaseRepository;
import com.example.jooq.models.tables.pojos.Test;
import com.example.jooq.models.tables.pojos.TestTestfield;
import org.jooq.DSLContext;

public class DatabaseRepositoryImpl implements DatabaseRepository {

    private DatabaseDataSource databaseDataSource = new DatabaseDataSourceImpl();

    @Override
    public Test create(DSLContext context, String text) {
        return databaseDataSource.create(context, text);
    }

    @Override
    public TestTestfield createTest(DSLContext context, Long testId) {
        return databaseDataSource.createTest(context, testId);
    }
}
